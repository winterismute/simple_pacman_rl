import pygame
import csv
import random

FPS = 30

LEVEL_BLOCK_SIZE = 16
LEVEL_BLOCKS_X = 28
LEVEL_BLOCKS_Y = 31
SCREENWIDTH = LEVEL_BLOCKS_X * LEVEL_BLOCK_SIZE
SCREENHEIGHT = LEVEL_BLOCKS_Y * LEVEL_BLOCK_SIZE

PLAYER_HEIGHT = 16
PLAYER_WIDTH = 16

LEVEL_FOOD_OFFSET = 4
LEVEL_FOOD_SIZE = 8

LEVEL_PILL_SIZE = 8

BASE_SPEED = 80
INITIAL_PACMAN_SPEED = int(BASE_SPEED * 0.8)
INITIAL_GHOST_SPEED = int(BASE_SPEED * 0.75)

GHOST_WIDTH = 16
GHOST_HEIGHT = 16

pygame.init()
SCREEN = pygame.display.set_mode((SCREENWIDTH, SCREENHEIGHT))
pygame.display.set_caption('Pacman')

pacman_sheet = pygame.image.load('assets/sheet1_16.png').convert_alpha()


def extractSprite(sheet, x, y, w, h):
    rect = pygame.Rect(x, y, w, h)
    image = pygame.Surface(rect.size).convert()
    image.blit(sheet, (0, 0), rect)
    return image.convert_alpha()


sprites_pacman = {}
sprites_pacman['RIGHT'] = extractSprite(pacman_sheet, PLAYER_WIDTH * 10, PLAYER_HEIGHT * 1, PLAYER_WIDTH, PLAYER_HEIGHT)
sprites_pacman['LEFT'] = extractSprite(pacman_sheet, PLAYER_WIDTH * 9, PLAYER_HEIGHT * 2, PLAYER_WIDTH, PLAYER_HEIGHT)
sprites_pacman['DOWN'] = extractSprite(pacman_sheet, PLAYER_WIDTH * 10, PLAYER_HEIGHT * 4, PLAYER_WIDTH, PLAYER_HEIGHT)
sprites_pacman['UP'] = extractSprite(pacman_sheet, PLAYER_WIDTH * 9, PLAYER_HEIGHT * 5, PLAYER_WIDTH, PLAYER_HEIGHT)

sprite_block = pygame.image.load('assets/block_16.png').convert_alpha()

sprite_food = extractSprite(pacman_sheet, LEVEL_BLOCK_SIZE * 13 + LEVEL_BLOCK_SIZE / 2,
                            LEVEL_BLOCK_SIZE * 8 + LEVEL_BLOCK_SIZE / 2,
                            LEVEL_FOOD_SIZE, LEVEL_FOOD_SIZE)
sprite_pill = extractSprite(pacman_sheet, LEVEL_BLOCK_SIZE * 13 + LEVEL_BLOCK_SIZE / 2,
                            LEVEL_BLOCK_SIZE * 8,
                            LEVEL_PILL_SIZE, LEVEL_PILL_SIZE)


sprites_ghost = {}
sprites_ghost['RIGHT'] = extractSprite(pacman_sheet, GHOST_WIDTH * 12, GHOST_HEIGHT * 0, GHOST_WIDTH, GHOST_HEIGHT)
sprites_ghost['DOWN'] = extractSprite(pacman_sheet, GHOST_WIDTH * 12, GHOST_HEIGHT * 2, GHOST_WIDTH, GHOST_HEIGHT)
sprites_ghost['LEFT'] = extractSprite(pacman_sheet, GHOST_WIDTH * 12, GHOST_HEIGHT * 4, GHOST_WIDTH, GHOST_HEIGHT)
sprites_ghost['UP'] = extractSprite(pacman_sheet, GHOST_WIDTH * 12, GHOST_HEIGHT * 6, GHOST_WIDTH, GHOST_HEIGHT)
sprites_ghost['SCARED'] = extractSprite(pacman_sheet, GHOST_WIDTH * 3, GHOST_HEIGHT * 2, GHOST_WIDTH, GHOST_HEIGHT)

# action_id == 0: do nothing
# action_id == 1: right
# action_id == 2: down
# action_id == 3: left
# action_id == 4: up
action_id_to_facing_string = ['NONE', 'RIGHT', 'DOWN', 'LEFT', 'UP']


class PacmanLevel:
    def __init__(self, filename, bsize):
        self.blockSize = bsize
        self.levelData = {}
        self.nFood = 0
        self.ghostSpawnPoint = None
        self.ghostHousPoints = []
        with open(filename) as csvfile:
            data = csv.reader(csvfile)
            currentY = 0
            for row in data:
                if currentY == 0:
                    self.widthInBlocks = len(row)
                else:
                    assert(len(row) == self.widthInBlocks)
                currentX = 0
                for el in row:
                    if el == '#':
                        self.levelData[(currentX, currentY)] = {'type': 'wall'}
                    elif el == ' ':
                        self.levelData[(currentX, currentY)] = {'type': 'food'}
                        self.nFood += 1
                    elif el == 'S':
                        self.levelData[(currentX, currentY)] = {'type': 'pill'}
                        self.nFood += 1
                    elif el == 'G':
                        self.ghostSpawnPoint = (currentX * self.blockSize, currentY * self.blockSize)
                    elif el == 'H':
                        self.ghostHousPoints.append((currentX * self.blockSize, currentY * self.blockSize))
                    currentX += 1
                currentY += 1
            self.heightInBlocks = currentY
            print('Loaded level {}. HightB: {} WidthB: {}'.format(filename, self.heightInBlocks, self.widthInBlocks))

        r = pygame.Rect(0, 0, self.widthInBlocks * self.blockSize, self.heightInBlocks * self.blockSize)
        self.onlySprite = pygame.Surface(r.size).convert()
        for x in range(self.widthInBlocks):
            for y in range(self.heightInBlocks):
                e = self.levelData.get((x, y), None)
                if e is not None and e['type'] == 'wall':
                    self.onlySprite.blit(sprite_block, (x * self.blockSize, y * self.blockSize))

    def getXYPos(self, rect, getMultiple=True):
        out = []
        # DO INTEGER DIVISION!
        blockx = int(rect.x / self.blockSize)
        blocky = int(rect.y / self.blockSize)
        out.append((blockx, blocky))
        if getMultiple:
            if ((int(rect.x) % self.blockSize) + rect.w) > self.blockSize:
                out.append((blockx + 1, blocky))
            if ((int(rect.y) % self.blockSize) + rect.h) > self.blockSize:
                out.append((blockx, blocky + 1))
        return out

    def rectCollidesWithWall(self, rect, checkMultiple=True):
        # print('Check collide rect {}'.format(rect))
        pairs = self.getXYPos(rect, checkMultiple)
        for p in pairs:
            # print('Checking pair {}'.format(p))
            e = self.levelData.get(p, None)
            if e is not None and e['type'] == 'wall':
                # print('Colliding at {}'.format(p))
                return rect.clip(pygame.Rect(p[0] * self.blockSize, p[1] * self.blockSize, self.blockSize, self.blockSize))
        return pygame.Rect(0, 0, 0, 0)

    def eatCollidingFood(self, rect):
        pairs = self.getXYPos(rect)
        for p in pairs:
            e = self.levelData.get(p, None)
            if e is not None and (e['type'] == 'food' or e['type'] == 'pill'):
                # print('Colliding with food')
                overlap = rect.clip(pygame.Rect(p[0] * self.blockSize + LEVEL_FOOD_OFFSET,
                                                p[1] * self.blockSize + LEVEL_FOOD_OFFSET,
                                                LEVEL_FOOD_SIZE, LEVEL_FOOD_SIZE))
                if overlap.width != 0 or overlap.height != 0:
                    self.levelData[p] = None
                    self.nFood -= 1
                    return e['type']  # Only eat the immediately colliding food
        return None

    def draw(self):
        SCREEN.blit(self.onlySprite, (0, 0))
        # blit food on top
        for x in range(self.widthInBlocks):
            for y in range(self.heightInBlocks):
                e = self.levelData.get((x, y), None)
                if e is not None:
                    if e['type'] == 'food':
                        SCREEN.blit(sprite_food, (x * self.blockSize + LEVEL_FOOD_OFFSET, y * self.blockSize + LEVEL_FOOD_OFFSET))
                    elif e['type'] == 'pill':
                        SCREEN.blit(sprite_pill, (x * self.blockSize + LEVEL_FOOD_OFFSET, y * self.blockSize + LEVEL_FOOD_OFFSET))


def wrapSprite(s, w, h, minx, miny, maxx, maxy):
    if s.px < (minx - w):
        s.px = maxx - s.px - w
    elif s.px > maxx:
        s.px -= maxx
    elif s.py < (miny - h):
        s.py = maxy - s.py - h
    elif s.py > maxy:
        s.py -= maxy


class BasicGhostEntity:
    def __init__(self, x, y, inHouse, startState='LEFT', startSpeed=100):
        self.px = x
        self.py = y
        # self.sprites = _sprites
        self.speed = startSpeed
        self.vx = 0
        self.vy = 0
        self.state = startState
        self.framesBeforeRepath = FPS * 3
        self.foodBeforeLeaving = 30 if inHouse else 0
        self.isScared = False

    def update(self, dt, level):
        if self.state is None or self.framesBeforeRepath == 0:
            if self.foodBeforeLeaving > 0:
                if self.state == 'DOWN':
                    self.state = 'UP'
                else:
                    self.state = 'DOWN'
            else:
                self.state = action_id_to_facing_string[random.randint(1, 4)]
            self.framesBeforeRepath = FPS * 3
        else:
            self.framesBeforeRepath -= 1

        if self.state == 'RIGHT':
            self.vx = self.speed
            self.vy = 0
        elif self.state == 'LEFT':
            self.vx = -self.speed
            self.vy = 0
        elif self.state == 'UP':
            self.vy = -self.speed
            self.vx = 0
        elif self.state == 'DOWN':
            self.vy = self.speed
            self.vx = 0
        self.px += self.vx * dt
        self.py += self.vy * dt

        outRect = level.rectCollidesWithWall(pygame.Rect(self.px, self.py, GHOST_WIDTH, GHOST_HEIGHT))
        # print('Clip rect {}'.format(outRect))
        if outRect.width > 0:
            if self.vx > 0:
                self.px -= outRect.width
            elif self.vx < 0:
                self.px += outRect.width
            self.framesBeforeRepath = 0
        if outRect.height > 0:
            if self.vy > 0:
                self.py -= outRect.height
            elif self.vy < 0:
                self.py += outRect.height
            self.framesBeforeRepath = 0

    def draw(self):
        SCREEN.blit(sprites_ghost['SCARED'] if self.isScared and self.foodBeforeLeaving == 0 else sprites_ghost[self.state], (self.px, self.py))


class PacmanEntity:
    def __init__(self, x, y, _sprites, speed=70):
        self.px = x
        self.py = y
        # self.prevx = x
        # self.prevy = y
        # self.sprites = _sprites
        self.speed = speed
        self.vx = 0
        self.vy = 0
        self.state = 'RIGHT'
        self.reqState = None
        self.reqStateFrames = 0
        self.framesSuper = 0

    def processInput(self, action_id):
        fs = action_id_to_facing_string[action_id]
        if fs == self.state:
            return
        if action_id == 1:
            if self.state == 'LEFT':
                self.state = 'RIGHT'
            else:
                self.reqState = 'RIGHT'
                self.reqStateFrames = FPS / 3
        elif action_id == 2:
            if self.state == 'UP':
                self.state = 'DOWN'
            else:
                self.reqState = 'DOWN'
                self.reqStateFrames = FPS / 3
        elif action_id == 3:
            if self.state == 'RIGHT':
                self.state = 'LEFT'
            else:
                self.reqState = 'LEFT'
                self.reqStateFrames = FPS / 3
        elif action_id == 4:
            if self.state == 'DOWN':
                self.state = 'UP'
            else:
                self.reqState = 'UP'
                self.reqStateFrames = FPS / 3

    def update(self, dt, level, houseGhosts, freeGhosts):
        eaten = level.eatCollidingFood(pygame.Rect(self.px, self.py, PLAYER_WIDTH, PLAYER_HEIGHT))
        if eaten is not None:
            if len(houseGhosts) > 0:
                houseGhosts[0].foodBeforeLeaving -= 1
                if houseGhosts[0].foodBeforeLeaving == 0:
                    # Remove it from the house
                    g = houseGhosts.pop(0)
                    g.px = level.ghostSpawnPoint[0]
                    g.py = level.ghostSpawnPoint[1]
                    freeGhosts.append(g)
            if eaten == 'pill':
                self.framesSuper = FPS * 7

        # print('PACMAN update dt {}'.format(dt))
        if self.state is not None:
            if self.state == 'RIGHT':
                self.vx = self.speed
                self.vy = 0
            elif self.state == 'LEFT':
                self.vx = -self.speed
                self.vy = 0
            elif self.state == 'UP':
                self.vy = -self.speed
                self.vx = 0
            elif self.state == 'DOWN':
                self.vy = self.speed
                self.vx = 0
        self.px += self.vx * dt
        self.py += self.vy * dt

        outRect = level.rectCollidesWithWall(pygame.Rect(self.px, self.py, PLAYER_WIDTH, PLAYER_HEIGHT))
        # print('Clip rect {}'.format(outRect))
        if outRect.width > 0:
            if self.vx > 0:
                self.px -= outRect.width
            elif self.vx < 0:
                self.px += outRect.width
        if outRect.height > 0:
            if self.vy > 0:
                self.py -= outRect.height
            elif self.vy < 0:
                self.py += outRect.height

        # print('DONE update state')
        coll_bias = 0
        if self.reqState is not None and self.reqStateFrames > 0:
            self.reqStateFrames -= 1
            newPos = pygame.Rect(self.px + coll_bias, self.py + coll_bias, PLAYER_WIDTH - coll_bias, PLAYER_HEIGHT - coll_bias)
            if self.reqState == 'RIGHT':
                newPos.x += level.blockSize
            elif self.reqState == 'LEFT':
                newPos.x -= level.blockSize
            elif self.reqState == 'UP':
                newPos.y -= level.blockSize
            elif self.reqState == 'DOWN':
                newPos.y += level.blockSize
            reqRect = level.rectCollidesWithWall(newPos, False)
            # print('reqRect {}'.format(reqRect))
            if reqRect.width == 0 and reqRect.height == 0:
                self.px = int(self.px / level.blockSize) * level.blockSize
                self.py = int(self.py / level.blockSize) * level.blockSize
                # print('OK reqState newPos: {}'.format(newPos))
                self.state = self.reqState
                self.reqState = None

    def draw(self):
        SCREEN.blit(sprites_pacman[self.state], (self.px, self.py))


class PacmanGameState:
    def __init__(self, readKeyEvents, seed=None):
        self.FPSClock = pygame.time.Clock()
        self.randomSeed = seed
        random.seed(self.randomSeed)
        self.score = self.playerIndex = self.loopIter = 0
        self.readKeys = readKeyEvents
        px = LEVEL_BLOCK_SIZE
        py = LEVEL_BLOCK_SIZE
        self.level = PacmanLevel('assets/level2.csv', LEVEL_BLOCK_SIZE)
        self.player = PacmanEntity(px, py, sprites_pacman, INITIAL_PACMAN_SPEED)
        self.freeGhosts = []
        self.houseGhosts = []
        # Spawn first free ghost
        gsp = self.level.ghostSpawnPoint
        g = BasicGhostEntity(gsp[0], gsp[1], False, 'LEFT', INITIAL_GHOST_SPEED)
        self.freeGhosts.append(g)
        # Spawn ghosts in the house
        for hp in self.level.ghostHousPoints:
            g = BasicGhostEntity(hp[0], hp[1], True, 'DOWN', INITIAL_GHOST_SPEED)
            self.houseGhosts.append(g)
        self.isGameOver = False

    def draw(self):
        self.level.draw()
        self.player.draw()
        for g in self.freeGhosts:
            g.draw()
        for g in self.houseGhosts:
            g.draw()

    def step(self, action_id=0):
        # print ("FPS" , FPSClock.get_fps())
        dt = self.FPSClock.tick(FPS) / 1000.0

        reward = 0.1
        ended = False

        if not self.isGameOver:
            # Process events
            newActionID = 0
            if self.readKeys:
                events = pygame.event.get()
                for event in events:
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_RIGHT:
                            # print('Pressed RIGHT')
                            newActionID = 1
                            break
                        elif event.key == pygame.K_DOWN:
                            # print('Pressed DOWN')
                            newActionID = 2
                            break
                        elif event.key == pygame.K_LEFT:
                            # print('Pressed LEFT')
                            newActionID = 3
                            break
                        elif event.key == pygame.K_UP:
                            # print('Pressed UP')
                            newActionID = 4
                            break
            else:
                pygame.event.pump()
                newActionID = action_id

            self.player.processInput(newActionID)

            # Update player
            self.player.update(dt, self.level, self.houseGhosts, self.freeGhosts)
            self.player.framesSuper -= 1
            wrapSprite(self.player, PLAYER_WIDTH, PLAYER_HEIGHT,
                       0, 0, LEVEL_BLOCK_SIZE * LEVEL_BLOCKS_X, LEVEL_BLOCK_SIZE * LEVEL_BLOCKS_Y)
            # Check game over for food
            if self.level.nFood == 0:
                self.isGameOver = True
            playerRect = pygame.Rect(self.player.px, self.player.py, PLAYER_WIDTH, PLAYER_HEIGHT)
            # Update ghosts not in the house
            for g in self.freeGhosts:
                g.isScared = self.player.framesSuper > 0
                g.update(dt, self.level)
                wrapSprite(g, GHOST_WIDTH, GHOST_HEIGHT,
                           0, 0, LEVEL_BLOCK_SIZE * LEVEL_BLOCKS_X, LEVEL_BLOCK_SIZE * LEVEL_BLOCKS_Y)
                overlap = playerRect.clip(pygame.Rect(g.px, g.py, GHOST_WIDTH, GHOST_HEIGHT))
                if overlap.width > 0 or overlap.height > 0:
                    # Collision with a ghost running around
                    if g.isScared:
                        g.isScared = False  # TODO: check this
                        nGH = len(self.houseGhosts)
                        if nGH == 3:  # House is maxed - spawn it outside
                            g.px = self.level.ghostSpawnPoint[0]
                            g.py = self.level.ghostSpawnPoint[1]
                            g.framesBeforeRepath = 0
                        else:  # Spawn it inside the house
                            g.px = self.level.ghostHousPoints[2 - nGH][0]
                            g.py = self.level.ghostHousPoints[2 - nGH][1]
                            g.foodBeforeLeaving = 30
                            g.state = 'DOWN'
                            g.framesBeforeRepath = FPS * 3
                    else:
                        self.isGameOver = True
                        break
            # Separate update for the ghosts in the house, no collisions with player
            for g in self.houseGhosts:
                g.isScared = self.player.framesSuper > 0
                g.update(dt, self.level)
                wrapSprite(g, GHOST_WIDTH, GHOST_HEIGHT,
                           0, 0, LEVEL_BLOCK_SIZE * LEVEL_BLOCKS_X, LEVEL_BLOCK_SIZE * LEVEL_BLOCKS_Y)
            ended = self.isGameOver

        # Draw
        # SCREEN.fill((0, 0, 0))
        self.draw()

        image_data = pygame.surfarray.array3d(pygame.display.get_surface())
        pygame.display.update()

        return image_data, reward, ended
