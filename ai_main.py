#!/usr/bin/env python
import sys
sys.path.append("game/")
import pacman_main as game


def showHelp():
    print('USAGE: ai_main.py [play|train|run]')
    print('play: play level')
    print('train: start training a model')
    print('run: run a pre-trained model')
    print('Default is: play')


def main(args):
    cmd = 'play'
    if len(args) > 0:
        if args[0] != 'play' and args[0] != 'train' and args[0] != 'run':
            print('Unrecognized command: {}'.format(args[0]))
            showHelp()
            return
        cmd = args[0]

    if cmd == 'play':
        game_state = game.PacmanGameState(True)
        ended = False
        while not ended:
            img, reward, ended = game_state.step(0)
    elif cmd == 'train':
        pass
    elif cmd == 'run':
        pass


if __name__ == "__main__":
    main(sys.argv[1:])
